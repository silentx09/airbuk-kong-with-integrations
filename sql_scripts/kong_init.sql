--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: delete_expired_cluster_events(); Type: FUNCTION; Schema: public; Owner: operator
--

CREATE FUNCTION public.delete_expired_cluster_events() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
          DELETE FROM "cluster_events"
                WHERE "expire_at" <= CURRENT_TIMESTAMP AT TIME ZONE 'UTC';
          RETURN NEW;
        END;
      $$;


ALTER FUNCTION public.delete_expired_cluster_events() OWNER TO operator;

--
-- Name: sync_tags(); Type: FUNCTION; Schema: public; Owner: operator
--

CREATE FUNCTION public.sync_tags() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
        BEGIN
          IF (TG_OP = 'TRUNCATE') THEN
            DELETE FROM tags WHERE entity_name = TG_TABLE_NAME;
            RETURN NULL;
          ELSIF (TG_OP = 'DELETE') THEN
            DELETE FROM tags WHERE entity_id = OLD.id;
            RETURN OLD;
          ELSE

          -- Triggered by INSERT/UPDATE
          -- Do an upsert on the tags table
          -- So we don't need to migrate pre 1.1 entities
          INSERT INTO tags VALUES (NEW.id, TG_TABLE_NAME, NEW.tags)
          ON CONFLICT (entity_id) DO UPDATE
                  SET tags=EXCLUDED.tags;
          END IF;
          RETURN NEW;
        END;
      $$;


ALTER FUNCTION public.sync_tags() OWNER TO operator;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: acls; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.acls (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    consumer_id uuid,
    "group" text,
    cache_key text
);


ALTER TABLE public.acls OWNER TO operator;

--
-- Name: apis; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.apis (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(3) with time zone),
    name text,
    upstream_url text,
    preserve_host boolean NOT NULL,
    retries smallint DEFAULT 5,
    https_only boolean,
    http_if_terminated boolean,
    hosts text,
    uris text,
    methods text,
    strip_uri boolean,
    upstream_connect_timeout integer,
    upstream_send_timeout integer,
    upstream_read_timeout integer
);


ALTER TABLE public.apis OWNER TO operator;

--
-- Name: basicauth_credentials; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.basicauth_credentials (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    consumer_id uuid,
    username text,
    password text
);


ALTER TABLE public.basicauth_credentials OWNER TO operator;

--
-- Name: certificates; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.certificates (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    cert text,
    key text,
    tags text[]
);


ALTER TABLE public.certificates OWNER TO operator;

--
-- Name: cluster_ca; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.cluster_ca (
    pk boolean NOT NULL,
    key text NOT NULL,
    cert text NOT NULL,
    CONSTRAINT cluster_ca_pk_check CHECK ((pk = true))
);


ALTER TABLE public.cluster_ca OWNER TO operator;

--
-- Name: cluster_events; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.cluster_events (
    id uuid NOT NULL,
    node_id uuid NOT NULL,
    at timestamp with time zone NOT NULL,
    nbf timestamp with time zone,
    expire_at timestamp with time zone NOT NULL,
    channel text,
    data text
);


ALTER TABLE public.cluster_events OWNER TO operator;

--
-- Name: consumers; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.consumers (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    username text,
    custom_id text,
    tags text[]
);


ALTER TABLE public.consumers OWNER TO operator;

--
-- Name: hmacauth_credentials; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.hmacauth_credentials (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    consumer_id uuid,
    username text,
    secret text
);


ALTER TABLE public.hmacauth_credentials OWNER TO operator;

--
-- Name: jwt_secrets; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.jwt_secrets (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    consumer_id uuid,
    key text,
    secret text,
    algorithm text,
    rsa_public_key text
);


ALTER TABLE public.jwt_secrets OWNER TO operator;

--
-- Name: keyauth_credentials; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.keyauth_credentials (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    consumer_id uuid,
    key text
);


ALTER TABLE public.keyauth_credentials OWNER TO operator;

--
-- Name: locks; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.locks (
    key text NOT NULL,
    owner text,
    ttl timestamp with time zone
);


ALTER TABLE public.locks OWNER TO operator;

--
-- Name: oauth2_authorization_codes; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.oauth2_authorization_codes (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    credential_id uuid,
    service_id uuid,
    code text,
    authenticated_userid text,
    scope text,
    ttl timestamp with time zone
);


ALTER TABLE public.oauth2_authorization_codes OWNER TO operator;

--
-- Name: oauth2_credentials; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.oauth2_credentials (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    name text,
    consumer_id uuid,
    client_id text,
    client_secret text,
    redirect_uris text[]
);


ALTER TABLE public.oauth2_credentials OWNER TO operator;

--
-- Name: oauth2_tokens; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.oauth2_tokens (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    credential_id uuid,
    service_id uuid,
    access_token text,
    refresh_token text,
    token_type text,
    expires_in integer,
    authenticated_userid text,
    scope text,
    ttl timestamp with time zone
);


ALTER TABLE public.oauth2_tokens OWNER TO operator;

--
-- Name: plugins; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.plugins (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    name text NOT NULL,
    consumer_id uuid,
    service_id uuid,
    route_id uuid,
    api_id uuid,
    config jsonb NOT NULL,
    enabled boolean NOT NULL,
    cache_key text,
    run_on text,
    protocols text[],
    tags text[]
);


ALTER TABLE public.plugins OWNER TO operator;

--
-- Name: ratelimiting_metrics; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.ratelimiting_metrics (
    identifier text NOT NULL,
    period text NOT NULL,
    period_date timestamp with time zone NOT NULL,
    service_id uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    route_id uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    value integer
);


ALTER TABLE public.ratelimiting_metrics OWNER TO operator;

--
-- Name: response_ratelimiting_metrics; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.response_ratelimiting_metrics (
    identifier text NOT NULL,
    period text NOT NULL,
    period_date timestamp with time zone NOT NULL,
    service_id uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    route_id uuid DEFAULT '00000000-0000-0000-0000-000000000000'::uuid NOT NULL,
    value integer
);


ALTER TABLE public.response_ratelimiting_metrics OWNER TO operator;

--
-- Name: routes; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.routes (
    id uuid NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    service_id uuid,
    protocols text[],
    methods text[],
    hosts text[],
    paths text[],
    regex_priority bigint,
    strip_path boolean,
    preserve_host boolean,
    name text,
    snis text[],
    sources jsonb[],
    destinations jsonb[],
    tags text[]
);


ALTER TABLE public.routes OWNER TO operator;

--
-- Name: schema_meta; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.schema_meta (
    key text NOT NULL,
    subsystem text NOT NULL,
    last_executed text,
    executed text[],
    pending text[]
);


ALTER TABLE public.schema_meta OWNER TO operator;

--
-- Name: services; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.services (
    id uuid NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    name text,
    retries bigint,
    protocol text,
    host text,
    port bigint,
    path text,
    connect_timeout bigint,
    write_timeout bigint,
    read_timeout bigint,
    tags text[]
);


ALTER TABLE public.services OWNER TO operator;

--
-- Name: snis; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.snis (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(0) with time zone),
    name text NOT NULL,
    certificate_id uuid,
    tags text[]
);


ALTER TABLE public.snis OWNER TO operator;

--
-- Name: tags; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.tags (
    entity_id uuid NOT NULL,
    entity_name text,
    tags text[]
);


ALTER TABLE public.tags OWNER TO operator;

--
-- Name: targets; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.targets (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(3) with time zone),
    upstream_id uuid,
    target text NOT NULL,
    weight integer NOT NULL,
    tags text[]
);


ALTER TABLE public.targets OWNER TO operator;

--
-- Name: ttls; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.ttls (
    primary_key_value text NOT NULL,
    primary_uuid_value uuid,
    table_name text NOT NULL,
    primary_key_name text NOT NULL,
    expire_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ttls OWNER TO operator;

--
-- Name: upstreams; Type: TABLE; Schema: public; Owner: operator
--

CREATE TABLE public.upstreams (
    id uuid NOT NULL,
    created_at timestamp with time zone DEFAULT timezone('UTC'::text, ('now'::text)::timestamp(3) with time zone),
    name text,
    hash_on text,
    hash_fallback text,
    hash_on_header text,
    hash_fallback_header text,
    hash_on_cookie text,
    hash_on_cookie_path text,
    slots integer NOT NULL,
    healthchecks jsonb,
    tags text[]
);


ALTER TABLE public.upstreams OWNER TO operator;

--
-- Data for Name: acls; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.acls (id, created_at, consumer_id, "group", cache_key) FROM stdin;
\.


--
-- Data for Name: apis; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.apis (id, created_at, name, upstream_url, preserve_host, retries, https_only, http_if_terminated, hosts, uris, methods, strip_uri, upstream_connect_timeout, upstream_send_timeout, upstream_read_timeout) FROM stdin;
\.


--
-- Data for Name: basicauth_credentials; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.basicauth_credentials (id, created_at, consumer_id, username, password) FROM stdin;
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.certificates (id, created_at, cert, key, tags) FROM stdin;
6737f2e2-3e56-4dba-9220-99aa84b78768	2019-05-31 07:38:29+00	-----BEGIN CERTIFICATE-----\nMIIEoDCCA4igAwIBAgIUZY1JhzPVqG+Dkmby1V4Lnjf7A3UwDQYJKoZIhvcNAQEL\nBQAwgYsxCzAJBgNVBAYTAlVTMRkwFwYDVQQKExBDbG91ZEZsYXJlLCBJbmMuMTQw\nMgYDVQQLEytDbG91ZEZsYXJlIE9yaWdpbiBTU0wgQ2VydGlmaWNhdGUgQXV0aG9y\naXR5MRYwFAYDVQQHEw1TYW4gRnJhbmNpc2NvMRMwEQYDVQQIEwpDYWxpZm9ybmlh\nMB4XDTE5MDUyNjA4NDYwMFoXDTM0MDUyMjA4NDYwMFowYjEZMBcGA1UEChMQQ2xv\ndWRGbGFyZSwgSW5jLjEdMBsGA1UECxMUQ2xvdWRGbGFyZSBPcmlnaW4gQ0ExJjAk\nBgNVBAMTHUNsb3VkRmxhcmUgT3JpZ2luIENlcnRpZmljYXRlMIIBIjANBgkqhkiG\n9w0BAQEFAAOCAQ8AMIIBCgKCAQEAykXeHAykFQAwYiJBMDICu/6x7e6AS4ASDtU2\nVF3lj0V2EHJjULibShc9Ei7WUM/LnHRLXaI0C1mpSNHrUnqtyI8orY93LVYtyTnp\nGE3A3DNkXX2DMhYsWIJqU2ffEV1jjyNM09InkoyV81VAFKF2PjBd9Zan9ZOSLp+G\nNi/gEgRw9x4mxrIRc53/GZh+yiNtavvCPjFshJzctnQJqdNzjVz5OViQWYOcz3Ge\nZn3ETHskvynGOeetr7JqR9nuVE0QfRYGSx0mSg5kejOX9vNhBKkuNYiYqFyG/5nQ\nfXW2c6l+EvunCwmb3T5mGYMCq73M5CrTeZHBr72MALt0WTfFCwIDAQABo4IBIjCC\nAR4wDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcD\nATAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBS2J4Fx9gBbX/Wi6mbhCgvlBajJhjAf\nBgNVHSMEGDAWgBQk6FNXXXw0QIep65TbuuEWePwppDBABggrBgEFBQcBAQQ0MDIw\nMAYIKwYBBQUHMAGGJGh0dHA6Ly9vY3NwLmNsb3VkZmxhcmUuY29tL29yaWdpbl9j\nYTAjBgNVHREEHDAaggwqLmFpcmJ1ay5jb22CCmFpcmJ1ay5jb20wOAYDVR0fBDEw\nLzAtoCugKYYnaHR0cDovL2NybC5jbG91ZGZsYXJlLmNvbS9vcmlnaW5fY2EuY3Js\nMA0GCSqGSIb3DQEBCwUAA4IBAQCFLrWAW59bDdgrY0XI1g0E2BKahPzlFd2GnWYI\nksDNOMJebYl3wd0KSTOToL97ufEFHFxiUQqNQOWTAIHxpvqlB7pOxY2KS+1A4iTE\n57C8lTmo1cpO+GA82gEsWQ86CXSksQPbfYCsKHfma3D/qn+WyvJ3TbZ3+1E4mx72\nv5zCTVnYhBWUxCKqjiI6DejWOaiqWuhcimxviG43WjLaimEZJWLaDhCuOxppXk3Q\nc4Zd4d6ddfXJq53WCWScC38zl1olsA7l5gkPjLIMvuC6yohZSQ9eWeJZFZHbnAMH\njNWUwTiBAAzONbB/tp3II21Re7kmK1gYE6iNJWpw4CFov0ll\n-----END CERTIFICATE-----	-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDKRd4cDKQVADBi\nIkEwMgK7/rHt7oBLgBIO1TZUXeWPRXYQcmNQuJtKFz0SLtZQz8ucdEtdojQLWalI\n0etSeq3Ijyitj3ctVi3JOekYTcDcM2RdfYMyFixYgmpTZ98RXWOPI0zT0ieSjJXz\nVUAUoXY+MF31lqf1k5Iun4Y2L+ASBHD3HibGshFznf8ZmH7KI21q+8I+MWyEnNy2\ndAmp03ONXPk5WJBZg5zPcZ5mfcRMeyS/KcY5562vsmpH2e5UTRB9FgZLHSZKDmR6\nM5f282EEqS41iJioXIb/mdB9dbZzqX4S+6cLCZvdPmYZgwKrvczkKtN5kcGvvYwA\nu3RZN8ULAgMBAAECggEAFjTjdJrirLePmEaAd6UV7cPBjwifJyrRULWRuGNyn28V\nzipwwtzxl4nDl6pytv6JMQ0HdSkoo5JdZnQ9bJmxWJJ2+c8FW4HZlvz3nUZ0BPjT\nbPwpmicbCUCQIcYGlGZ3VrCeb6bj10/96nASj/FGBP/UYPEAUZK4+stitbI93FxU\nZGt1cnZN+MZsMyt7eTxrOLpRWpdpkqhrx5+mEmRGrOBH28o1E0w8dYNjw5A24ZNH\n3ZQDDLL+Ps/B40KCOKF3F91PL7GoKdEDUJ9ELvwLSFJbC0J/NKFb2WLlUv8C4YkN\nWT9s4NDp1iGisy4SoI5K30wgliCIA6M+4OlKq77v2QKBgQDy13Vf/nitRq2tvIJ1\nH6acCJQ6ANuT27gv0Vydeswu5S7cQYGrjTKeMBS5vOIH1vNlFYgpJYHp/8FV0mvm\neADHtrBdm5jxRvAk4ucifDOVMQyvcSBUkZFYU1tPPrSCo7OcdyPDrnqpvw410ZiN\n/dxN+YML/m5f2wL05ILYzcsTzQKBgQDVO6qPbAtn+tPj3LAvJFqSyyIu77Uf2nTw\nEZ9r05Mhv08R5nA2wc8io37czQwMGGAYuZx3uw3P3NgKx7+ZYPzqHqhMiFkf3FRT\nYvmZcTSh3U1Q6v4dDY7tiNC2523FYyiNPL7MsDcKD9UlC0uPqYr8I17O6IH8LBG1\nPbtPC7SUNwKBgQCCUrTQDn3u5+ZfPbJ5bsH4LPZ3ZmxwnlLrfp0IepVHeYE7fQA8\n86q9kCofP8nB0U6P6g1Hn7RnwvSbvTBg3/fzNgzSP6XFnn9UHF1UeXkpUMhxAvIJ\n0JFC6IExNwKitX23s4yDAIkdvcW/hjsrwJhRVF1Ku4G6eYvNpzC9bzvT5QKBgF2g\nXTusTTXSxMcAV7vbUomx98h0KCtcWIwwsLc/pxLiVW2eqRbh8nlbIJEC149YBJnW\npgVMdLwYVCQnoNFP7L0/yNYR6FA9Bim3A7GEK4AHE7O7+uf21BMoR7yH0YMiDuQ1\nD/IATap1WTlyxDLIVuZvAWoIXUrEbuINwPjsQtXxAoGAMoOSReKO/RR+aRYQ4aZc\nj3sChz1tB4Po7xbeSo7h1FdnzVH/4NHzpj6tqQU/RHt35H7Aha+0HmER2gIoWsOU\nkIRSr5ZnoFAnRBestEcmOH2BNF9OHVbToy389+/6n3jngwC4kv61c/hBimaPheK7\nIKKfhK8akvHUS7dQEitneKo=\n-----END PRIVATE KEY-----	\N
\.


--
-- Data for Name: cluster_ca; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.cluster_ca (pk, key, cert) FROM stdin;
t	-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDSZgdkZcDDDgLO\nE/WlWpPiklgyNx9bVbSx8uE3u1qiqBuifespbSylpPdWzhGXttbX31SF1Y8uVBoy\nzEAJmARAZW7Ezovk9S7DMSmXAbuTSu8D1PxBh15ImvmVOstyuc78c3q5BZnl+AQU\n3pu8/imKJ/M0DLHoX03OmNz1rWxP3HAJzrwOieM1VFiY9uLfPKtbxnJhWorjZZ22\nai+WGIgn+l5HKqdvdU1YwqAo4uZEzjdp41TCfMbWcCFF/2laFNVDJ0msThisqyFJ\n4n6L6SLcn8II6nmcEEM8PvqGpw1pXOp5kKHggD/OJeGNZk3Zk0Wi3KkMxLmcJgH0\nxRTH1bljAgMBAAECggEAd2j2qxFg7JMoHmOPmBAnPIEIUz+bqU+kJRaOof3QO96j\nCH7kl3dhzoUo53g+3dckahJTOz3wbD/JOqlXctp3F0E6M7JyDTjmGFvvmcee+MKf\nkpIvtNUqOpTSL9LVosA37Glx7k5ygOVZE6sfwoyRcNqpmhHWauakFywEw95wSYXv\nY4xJND+cDjPfkuNUk6h7YmZqwpFhOtz340ZbwRGNX0tyX5XFS4L7uzSv6mDCvZYZ\n03URdtLmGxibDzV84vLo0eD+ulIlDAXVPwqEB9EPMC3Nd3xVAZgKvKoTje0g3YIS\n4yUs48OZBskXmITEbNm86FqIL8516RUIIz+x8SVcSQKBgQD5hisE2USxKvcd649U\ncNGuFeplhvjl/wbWXuM/XvKGXYbS15koGzPOLGhFJbNfWIKPzdf9XQuPcMBrt/9e\nKVEu9Wo1Ab+MT0Uhw8gqB7V2LxpTTt9hh6OeVF+UKSpDmWHi5RoxKimeqoHNvkVC\nuJ3m24Exyp8k4exRL5iMgikKpwKBgQDX2+lj8Ct5EhgIWmTCQfnthrWPtL1A5joo\nZ/tYJbZt7NTXDF/qZocAwGOOiRU2wGfuw+/U/dQSg13G8Qfh5EBUkRtxyws2DGW8\nGG317sy1ye7O6qZkBDqsg1pX0neSnblTPUXa/pbMPFeIaSH1kWt23gvdRUfklUJR\n4JMtxDd+5QKBgQC3ZsAz3iY5OmVMHpnI7OuRUxZn7V8AXgR+iFvrdjQ4QoIYctNl\nwSjAv68cM+u1hu7gF+JLnZojojIsep8+bIv5QfUAZPqPfKxJdwemrOfZqCFnXi+e\nFf5TH5zo36Dq8tAZ317zCBRzRMwHl3R86AlXZ7t8B3wVteDmg19S11CnPQKBgQCW\nXSYS67YcvDHpf644c2T5xKCs3iYHE1jn1KcNHBgcMqGAu6oEyi64o0u2el9AqH69\nn9ds8vPLgZ7NehEXlbwKLJqFhf7lQ3WKtSnD6vjGmUe081e8zAVjLoy8KGwKwbva\n9A+5ISdCVbZT24hAW3AUeVCibiRKYn60Mk/TzZ8q0QKBgFrlBRYIdxg9r9/FQwZt\nyIsLY0NBkUGol+vmTTkEXAtrmW2zI/QdT2jj9Nrw1u0+KrqZFUwnHRpL9NjMgiJB\nHfnJrZYaxslCaARfEi0nMJZfjQdJ9+B1rWL4oyG6Og7DoVtoET4RD3cY12Ita9Eg\nCbG84p83VfH5wtxCFepVri0P\n-----END PRIVATE KEY-----\n	-----BEGIN CERTIFICATE-----\nMIIDZjCCAk6gAwIBAgIRALeV/ymgD92FH93w9+WGVhkwDQYJKoZIhvcNAQELBQAw\nPDE6MDgGA1UEAwwxa29uZy1jbHVzdGVyLTQ4M2VhZjA1LWViNDMtNGRlZC1iNGYw\nLTBkNTMzOTRjZDc0YjAeFw0xOTA1MzEwNzAyMjFaFw0zOTA1MjYwNzAyMjFaMDwx\nOjA4BgNVBAMMMWtvbmctY2x1c3Rlci00ODNlYWYwNS1lYjQzLTRkZWQtYjRmMC0w\nZDUzMzk0Y2Q3NGIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDSZgdk\nZcDDDgLOE/WlWpPiklgyNx9bVbSx8uE3u1qiqBuifespbSylpPdWzhGXttbX31SF\n1Y8uVBoyzEAJmARAZW7Ezovk9S7DMSmXAbuTSu8D1PxBh15ImvmVOstyuc78c3q5\nBZnl+AQU3pu8/imKJ/M0DLHoX03OmNz1rWxP3HAJzrwOieM1VFiY9uLfPKtbxnJh\nWorjZZ22ai+WGIgn+l5HKqdvdU1YwqAo4uZEzjdp41TCfMbWcCFF/2laFNVDJ0ms\nThisqyFJ4n6L6SLcn8II6nmcEEM8PvqGpw1pXOp5kKHggD/OJeGNZk3Zk0Wi3KkM\nxLmcJgH0xRTH1bljAgMBAAGjYzBhMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/\nBAQDAgEGMB0GA1UdDgQWBBQtHbABmudsXvU37R5TqLp44w+XrjAfBgNVHSMEGDAW\ngBQtHbABmudsXvU37R5TqLp44w+XrjANBgkqhkiG9w0BAQsFAAOCAQEAdTeJhl1i\n6C4fgyhSRHIyeir69ezWX7+/rWnK1Bilg0v2vxiOvLujBVv8c8dKK0DxKBWNSi3y\nC8fE06ZRsDO7NrqZW4mefw5/B9x72iCAPIz1r1XKy9F7bzANEGnPViDQ8e59bi3w\nRCEiycsbOh4xbb3lKlhDWdCmA8v3rWpENO0j49QSA0IhxIb6tvKyqHEUoZa1abPq\nGzXYPDzEhsnnFQ7Iqc3p77Tv020pLlhsuc+JRok9FQE8g+aL16QV0HzoutYmx005\ntm8WmQS8XkRJt9l2UxknDATUKe7vYLcvyaMbSmzKCjD6f9NJJfGPdYNgE26bMrSm\noYrbCejUa7uzQw==\n-----END CERTIFICATE-----\n
\.


--
-- Data for Name: cluster_events; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.cluster_events (id, node_id, at, nbf, expire_at, channel, data) FROM stdin;
ffc8e739-48fb-46a5-9d12-cec3acaad3ab	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:09:06.813+00	\N	2019-06-01 19:09:06.813+00	invalidations	services:a9a21678-ad68-414c-8daf-2837c29f066d::::
52edd5b9-f259-41a4-8726-ca85d9c6edb7	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:09:15.48+00	\N	2019-06-01 19:09:15.48+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
719b198c-0558-4f6a-a7c7-3918bb8b69a6	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:09:15.481+00	\N	2019-06-01 19:09:15.481+00	invalidations	router:version
f46aa0aa-d9a7-42bd-8a15-0a7e99741371	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:09:30.871+00	\N	2019-06-01 19:09:30.871+00	invalidations	plugins:jwt::a9a21678-ad68-414c-8daf-2837c29f066d::
01611587-7bd4-406b-a50f-8cd78acda9a2	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:09:30.872+00	\N	2019-06-01 19:09:30.872+00	invalidations	plugins_map:version
7395a93a-9dd4-4d01-bfd8-dde21a39a3f9	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:11:54.368+00	\N	2019-06-01 19:11:54.368+00	invalidations	plugins:jwt::a9a21678-ad68-414c-8daf-2837c29f066d::
04a1990b-72bb-4889-8f8b-0ae4140e35b2	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:11:54.369+00	\N	2019-06-01 19:11:54.369+00	invalidations	plugins:jwt::a9a21678-ad68-414c-8daf-2837c29f066d::
1f86ecad-6614-4e7b-ac34-389b19b09b84	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:11:54.37+00	\N	2019-06-01 19:11:54.37+00	invalidations	plugins_map:version
4cdd7cab-dac9-471e-88fe-0f638504196b	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:03.113+00	\N	2019-06-01 19:12:03.113+00	invalidations	plugins:jwt::a9a21678-ad68-414c-8daf-2837c29f066d::
9ca9fae1-3b86-430e-b617-054d68004929	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:03.114+00	\N	2019-06-01 19:12:03.114+00	invalidations	plugins:jwt::a9a21678-ad68-414c-8daf-2837c29f066d::
9642cbf4-af33-428e-9b99-e1d92c90ada2	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:03.115+00	\N	2019-06-01 19:12:03.115+00	invalidations	plugins_map:version
15b3c704-6feb-4de1-870e-1915e8ed772e	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:30.254+00	\N	2019-06-01 19:12:30.254+00	invalidations	routes:a4243548-482b-4a62-9000-0a946877bfdd::::
e366615b-52d3-4c90-922f-37260b6f2585	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:30.255+00	\N	2019-06-01 19:12:30.255+00	invalidations	router:version
940731b5-a9c2-4abb-96dd-baeed19fe02a	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:39.335+00	\N	2019-06-01 19:12:39.335+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
e93a7be2-1cee-4e2d-a83e-64fc614f9c41	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:39.336+00	\N	2019-06-01 19:12:39.336+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
f9fec2df-c1eb-4a5f-8edc-25fd2afe61c4	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:39.337+00	\N	2019-06-01 19:12:39.337+00	invalidations	router:version
0741120e-6a7c-4ecb-b71a-c279a8a3abd1	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:12:47.425+00	\N	2019-06-01 19:12:47.425+00	invalidations	services:bbe83b0b-264b-4038-9d4d-089782d2c7d8::::
1fb6ed5f-ec76-4aef-8c3f-d83b5cb2a7d6	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:13:00.792+00	\N	2019-06-01 19:13:00.792+00	invalidations	plugins:jwt:2f4530a2-891d-41ba-80d4-fd64a5aba009::72f0554f-0623-48d5-bdfc-0a73fd56aa4c:
7e9c97d3-d7cc-40c6-9c3f-a4e1c8ab0d07	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:13:00.798+00	\N	2019-06-01 19:13:00.798+00	invalidations	plugins_map:version
05b12c90-d17b-4d40-a628-5c6ad1bb2065	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:13:35.594+00	\N	2019-06-01 19:13:35.594+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
3f3a0a54-05b9-446d-b649-640dea634515	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:13:35.595+00	\N	2019-06-01 19:13:35.595+00	invalidations	plugins_map:version
1396b375-a745-44f8-9138-38d06b931d0e	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:16:42.515+00	\N	2019-06-01 19:16:42.515+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
57a759ab-c885-42f8-b127-0324a0197f97	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:16:42.516+00	\N	2019-06-01 19:16:42.516+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
544bc9df-5979-48fc-b9f9-cf22db74df4a	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:16:42.517+00	\N	2019-06-01 19:16:42.517+00	invalidations	plugins_map:version
4cb3c41e-239b-4049-9e89-aa0ccc176e7f	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:25:16.183+00	\N	2019-06-01 19:25:16.183+00	invalidations	jwt_secrets:cx7qewMFfNOEzdadbIGOjiwphTZ2vttN::::
319e336c-435b-42fb-bbd0-7d1d8dde366e	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:27:49.102+00	\N	2019-06-01 19:27:49.102+00	invalidations	jwt_secrets:keycloak::::
5be1d470-9126-4146-b800-7c7ac4af2709	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:31:34.83+00	\N	2019-06-01 19:31:34.83+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
547ac8a6-f883-4dec-8cbe-6e98de1f6d5b	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:31:34.832+00	\N	2019-06-01 19:31:34.832+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
b27ad06f-2d62-4fa4-88f5-f91d0ec52b78	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:31:34.833+00	\N	2019-06-01 19:31:34.833+00	invalidations	plugins_map:version
62b796ae-c0a6-4942-b776-96be2c554152	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:31:53.949+00	\N	2019-06-01 19:31:53.949+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
27cc49f2-c378-45f8-8e17-35235842332c	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:31:53.95+00	\N	2019-06-01 19:31:53.95+00	invalidations	plugins_map:version
d349b608-d351-4850-8a41-e475f2d80b39	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:32:03.938+00	\N	2019-06-01 19:32:03.938+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
fadab6d5-d17a-424b-84ec-3f6442809504	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:32:03.939+00	\N	2019-06-01 19:32:03.939+00	invalidations	plugins_map:version
b34ac71c-1045-485f-8a7e-f91b47af48d5	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:32:45.402+00	\N	2019-06-01 19:32:45.402+00	invalidations	jwt_secrets:keycloak::::
3a219eff-5364-4f76-816c-7228395efc15	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:33:07.071+00	\N	2019-06-01 19:33:07.071+00	invalidations	jwt_secrets:OApcz2ODyfNXlTeljNkegn8fnbfmxhxp::::
5dbc767a-2f00-45dc-9b56-7d212f657374	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:51:26.625+00	\N	2019-06-01 19:51:26.625+00	invalidations	jwt_secrets:OApcz2ODyfNXlTeljNkegn8fnbfmxhxp::::
38d2d5f8-c974-46c4-8fc4-3b7d220cc30a	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:51:45.01+00	\N	2019-06-01 19:51:45.01+00	invalidations	jwt_secrets:https://keycloak.airbuk.com/auth/realms/master::::
f8f5e1bb-2171-4049-89f8-45ec01acecec	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:53:37.578+00	\N	2019-06-01 19:53:37.578+00	invalidations	plugins:jwt::a9a21678-ad68-414c-8daf-2837c29f066d::
4b7e9704-1e49-4e70-becb-50ce4b3ce8c2	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:53:37.579+00	\N	2019-06-01 19:53:37.579+00	invalidations	plugins_map:version
5749436d-cf99-43df-b2f0-ce2f421af7f5	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:54:24.114+00	\N	2019-06-01 19:54:24.114+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
922ae6fc-26a0-4222-b6c5-5667553b4b56	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:54:24.115+00	\N	2019-06-01 19:54:24.115+00	invalidations	plugins_map:version
969457a9-c874-4045-a394-dd6155d9ce11	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:55:14.46+00	\N	2019-06-01 19:55:14.46+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
0ba62614-b70d-4aa0-a0f4-61a803006e47	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:55:14.461+00	\N	2019-06-01 19:55:14.461+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
61053680-23c3-418d-a321-a03974319b88	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:55:14.462+00	\N	2019-06-01 19:55:14.462+00	invalidations	plugins_map:version
99a0227a-f786-4417-a583-79dff7740860	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:02.58+00	\N	2019-06-01 19:56:02.58+00	invalidations	plugins:request-transformer:2f4530a2-891d-41ba-80d4-fd64a5aba009:::
73d01f78-53d7-472b-ba44-2574d07c0aab	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:02.582+00	\N	2019-06-01 19:56:02.582+00	invalidations	plugins_map:version
2b33fb22-e533-478e-97f4-07dd439f58d4	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:35.922+00	\N	2019-06-01 19:56:35.922+00	invalidations	plugins:request-transformer:2f4530a2-891d-41ba-80d4-fd64a5aba009:::
45e52e3f-52d6-4f0a-b7a5-ba7a9df74021	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:35.924+00	\N	2019-06-01 19:56:35.924+00	invalidations	plugins_map:version
639d5ad5-bb83-4e13-a201-425b0f256e87	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:38.929+00	\N	2019-06-01 19:56:38.929+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
1fa8649e-5633-40ef-a90f-3cd91f6ec319	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:38.93+00	\N	2019-06-01 19:56:38.93+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
f72c117f-64f6-4b2d-85c5-22637628d2b6	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:56:38.931+00	\N	2019-06-01 19:56:38.931+00	invalidations	plugins_map:version
3dcd9a6c-c5e0-480c-baef-8436df7e9237	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:58:48.392+00	\N	2019-06-01 19:58:48.392+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
1d9e12c6-f296-4e82-a5ef-549f2b10070d	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:58:48.393+00	\N	2019-06-01 19:58:48.393+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
09f98aa6-73e4-4c44-abca-cb8b3b8211f8	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 18:58:48.394+00	\N	2019-06-01 19:58:48.394+00	invalidations	plugins_map:version
43da27cf-8544-4586-bee5-c982925d9da9	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:00:26.963+00	\N	2019-06-01 20:00:26.963+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
fb5a3675-f1cc-4cd2-877b-8219a3faa7d7	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:00:26.963+00	\N	2019-06-01 20:00:26.963+00	invalidations	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::
1b0d6b30-f005-4eea-93ee-506cf1d59558	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:00:26.964+00	\N	2019-06-01 20:00:26.964+00	invalidations	plugins_map:version
c9018f77-25aa-4960-8bed-ba3a07f05fa8	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:00:46.641+00	\N	2019-06-01 20:00:46.641+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
3823c0b5-6240-4770-b331-eca5f6d43744	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:00:46.642+00	\N	2019-06-01 20:00:46.642+00	invalidations	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::
bc417c96-49a1-4623-b933-c1c05a4bbfda	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:00:46.643+00	\N	2019-06-01 20:00:46.643+00	invalidations	plugins_map:version
8126c957-545e-4d0c-b3cb-8f4a4b89ce9f	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:02:38.045+00	\N	2019-06-01 20:02:38.045+00	invalidations	services:a9a21678-ad68-414c-8daf-2837c29f066d::::
cc2a2c5b-6d9f-4175-9e2e-6e6c7c677b14	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:02:38.046+00	\N	2019-06-01 20:02:38.046+00	invalidations	services:a9a21678-ad68-414c-8daf-2837c29f066d::::
bfcad441-e15a-4300-99f6-82ecf42df9e0	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:02:38.047+00	\N	2019-06-01 20:02:38.047+00	invalidations	router:version
5195ea4f-9e07-419b-b869-97c9f0c28ffe	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:02:45.804+00	\N	2019-06-01 20:02:45.804+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
a5460e2e-0db6-4c04-aa33-b0cbec0c1366	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:02:45.805+00	\N	2019-06-01 20:02:45.805+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
4c8648bc-b624-4930-b9f8-2a44b97660c7	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:02:45.806+00	\N	2019-06-01 20:02:45.806+00	invalidations	router:version
567f3ff6-d474-4aff-891e-309e19396610	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:00.454+00	\N	2019-06-01 20:03:00.454+00	invalidations	services:274a9154-6480-431f-ab37-a7a4cd8018df::::
720a08de-30bf-4895-8a7b-e11a8beac27a	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:00.455+00	\N	2019-06-01 20:03:00.455+00	invalidations	services:274a9154-6480-431f-ab37-a7a4cd8018df::::
53a3abda-c87c-45c7-8329-60234d4e50bd	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:00.456+00	\N	2019-06-01 20:03:00.456+00	invalidations	router:version
c6aca87c-c831-4b8e-ab93-71c8e5231f27	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:17.873+00	\N	2019-06-01 20:03:17.873+00	invalidations	services:274a9154-6480-431f-ab37-a7a4cd8018df::::
e59d86de-01f4-4bc9-bebf-3de39b468161	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:17.874+00	\N	2019-06-01 20:03:17.874+00	invalidations	services:274a9154-6480-431f-ab37-a7a4cd8018df::::
50856812-6b93-424b-a058-a050bc947ca2	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:17.875+00	\N	2019-06-01 20:03:17.875+00	invalidations	router:version
2ed68213-2034-40d5-925b-5553f88073b0	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:35.045+00	\N	2019-06-01 20:03:35.045+00	invalidations	services:274a9154-6480-431f-ab37-a7a4cd8018df::::
e70e5b99-86b7-456e-905c-4fe67668bc05	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:35.047+00	\N	2019-06-01 20:03:35.047+00	invalidations	services:274a9154-6480-431f-ab37-a7a4cd8018df::::
e2ca6847-0f8c-4d1c-92c5-1003eeeb8874	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:35.047+00	\N	2019-06-01 20:03:35.047+00	invalidations	router:version
92e7fad5-9ea1-4a6d-aa59-3a687511bbd0	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:41.188+00	\N	2019-06-01 20:03:41.188+00	invalidations	services:a9a21678-ad68-414c-8daf-2837c29f066d::::
e808c247-fc27-47f9-8965-3ec7d299186c	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:41.189+00	\N	2019-06-01 20:03:41.189+00	invalidations	services:a9a21678-ad68-414c-8daf-2837c29f066d::::
a3bd8c01-a16e-4c21-a534-8a01de679c80	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:41.19+00	\N	2019-06-01 20:03:41.19+00	invalidations	router:version
9b8f25f7-d954-49f4-8c5c-110f6a895d81	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:49.608+00	\N	2019-06-01 20:03:49.608+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
ce6a3cbd-1962-426e-b4b2-7c57b1581f2b	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:49.609+00	\N	2019-06-01 20:03:49.609+00	invalidations	routes:2dbbe332-d82d-481d-a95c-185486649b10::::
9ace9324-4570-4c87-9133-0524f7fff1b6	1e4905bf-94aa-400e-8a00-42fb3e299af8	2019-06-01 19:03:49.609+00	\N	2019-06-01 20:03:49.609+00	invalidations	router:version
\.


--
-- Data for Name: consumers; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.consumers (id, created_at, username, custom_id, tags) FROM stdin;
72f0554f-0623-48d5-bdfc-0a73fd56aa4c	2019-05-31 19:08:20+00	airbuk	airbuk	\N
\.


--
-- Data for Name: hmacauth_credentials; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.hmacauth_credentials (id, created_at, consumer_id, username, secret) FROM stdin;
\.


--
-- Data for Name: jwt_secrets; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.jwt_secrets (id, created_at, consumer_id, key, secret, algorithm, rsa_public_key) FROM stdin;
5633bfcb-6811-49c8-8bba-2e2cdc63d37c	2019-06-01 18:51:45+00	72f0554f-0623-48d5-bdfc-0a73fd56aa4c	https://keycloak.airbuk.com/auth/realms/master	-----BEGIN RSA PRIVATE KEY----- MIICmzCCAYMCBgFq9aPvlDANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMTkwNTI2MTkzMzQyWhcNMjkwNTI2MTkzNTIyWjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCCMSGk9adXoaOD9A8qAglNWYc4HnSJ1Clfb+ggKelgD3hkbfdvzTFGdQDC/iinJkcuTb4rzINOL+KBkwnhUGpcX9+ZpnH6/VUgV8pBquLIu4+AAu0zLfR1cF4gaYmCF/meR8DPs/WQOKEqb/4wijQUV9Jmxj1Fq/aFDJCPDEr/yb8MdW1B1DlEISOBXI+6Mf5paMVdR1JnmzKONJdyQvfoOiauPcrmDBq2gAiIfyrvhlL9I4Q5F4UGPzkMLEM+Xb4fZUiqMAR8X7kj+72wKUvXwdsZnSEf6Wk9OEi1PVehdi7CD7YGkFVBla77p/0dE7ulKjhMMmSfCuujxN2JbpQRAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAHB/M6xPCB6e5PmtyHj5gyJgzTrz0JNLxZ5NikZqEhacpFt2H3ZMYupXouPcbhbC2rM38aOprSltLg5W/SQ0DgR6TsSbvNgWW5IJimmP78cWnelX+zNVBVM148Z1ejt89bvCvp4nBVem/4UEJIfmxiY4PpAqNjWyo0fOLfCpVlgTiHgls9RTPEUHTJ/gnitD4+J+2N/NkOq58Mb2iEET9j7sfk0hXIvlDtRcFiiGJsb1O6myZ/kQdGdqmpiThkD7xUMeOeWR7wZ6ykJ6OlW9f76wn661gddsA7zmOIMjZOqsh80ro6t7yuK/DwHBZOfgClk4CgT9B6BalfanybKRKcY= -----END RSA PRIVATE KEY-----	RS256	-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgjEhpPWnV6Gjg/QPKgIJTVmHOB50idQpX2/oICnpYA94ZG33b80xRnUAwv4opyZHLk2+K8yDTi/igZMJ4VBqXF/fmaZx+v1VIFfKQariyLuPgALtMy30dXBeIGmJghf5nkfAz7P1kDihKm/+MIo0FFfSZsY9Rav2hQyQjwxK/8m/DHVtQdQ5RCEjgVyPujH+aWjFXUdSZ5syjjSXckL36Domrj3K5gwatoAIiH8q74ZS/SOEOReFBj85DCxDPl2+H2VIqjAEfF+5I/u9sClL18HbGZ0hH+lpPThItT1XoXYuwg+2BpBVQZWu+6f9HRO7pSo4TDJknwrro8TdiW6UEQIDAQAB\n-----END PUBLIC KEY-----
\.


--
-- Data for Name: keyauth_credentials; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.keyauth_credentials (id, created_at, consumer_id, key) FROM stdin;
\.


--
-- Data for Name: locks; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.locks (key, owner, ttl) FROM stdin;
\.


--
-- Data for Name: oauth2_authorization_codes; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.oauth2_authorization_codes (id, created_at, credential_id, service_id, code, authenticated_userid, scope, ttl) FROM stdin;
\.


--
-- Data for Name: oauth2_credentials; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.oauth2_credentials (id, created_at, name, consumer_id, client_id, client_secret, redirect_uris) FROM stdin;
\.


--
-- Data for Name: oauth2_tokens; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.oauth2_tokens (id, created_at, credential_id, service_id, access_token, refresh_token, token_type, expires_in, authenticated_userid, scope, ttl) FROM stdin;
\.


--
-- Data for Name: plugins; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.plugins (id, created_at, name, consumer_id, service_id, route_id, api_id, config, enabled, cache_key, run_on, protocols, tags) FROM stdin;
fc66dbbb-75be-4829-bec7-01c3ac86c0ea	2019-06-01 18:54:24+00	request-transformer	\N	274a9154-6480-431f-ab37-a7a4cd8018df	\N	\N	{"add": {"body": [], "headers": [], "querystring": []}, "append": {"body": [], "headers": [], "querystring": []}, "remove": {"body": [], "headers": ["Authorization", "authorization"], "querystring": []}, "rename": {"body": [], "headers": [], "querystring": []}, "replace": {"body": [], "headers": [], "querystring": []}, "http_method": null}	t	plugins:request-transformer::274a9154-6480-431f-ab37-a7a4cd8018df::	first	{http,https}	\N
a6997c80-5240-4e6c-8b86-3b449969c67d	2019-06-01 18:32:03+00	jwt	\N	274a9154-6480-431f-ab37-a7a4cd8018df	\N	\N	{"anonymous": null, "cookie_names": [], "key_claim_name": "iss", "uri_param_names": ["jwt"], "claims_to_verify": null, "run_on_preflight": true, "secret_is_base64": false, "maximum_expiration": 0}	t	plugins:jwt::274a9154-6480-431f-ab37-a7a4cd8018df::	first	{http,https}	\N
\.


--
-- Data for Name: ratelimiting_metrics; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.ratelimiting_metrics (identifier, period, period_date, service_id, route_id, value) FROM stdin;
\.


--
-- Data for Name: response_ratelimiting_metrics; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.response_ratelimiting_metrics (identifier, period, period_date, service_id, route_id, value) FROM stdin;
\.


--
-- Data for Name: routes; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.routes (id, created_at, updated_at, service_id, protocols, methods, hosts, paths, regex_priority, strip_path, preserve_host, name, snis, sources, destinations, tags) FROM stdin;
2f4530a2-891d-41ba-80d4-fd64a5aba009	2019-05-31 19:37:33+00	2019-06-01 17:33:12+00	274a9154-6480-431f-ab37-a7a4cd8018df	{https,http}	{POST,GET}	{}	{/rest}	0	f	f	REST	\N	\N	\N	\N
3c29d8e0-17ee-496d-873d-e42194f2ced1	2019-05-31 19:06:08+00	2019-06-01 17:34:54+00	274a9154-6480-431f-ab37-a7a4cd8018df	{https,http}	{POST}	{}	{/graphql}	0	f	f	GraphQL	\N	\N	\N	\N
2dbbe332-d82d-481d-a95c-185486649b10	2019-06-01 18:09:15+00	2019-06-01 19:03:49+00	a9a21678-ad68-414c-8daf-2837c29f066d	{http,https}	{}	{}	{/chuck}	0	t	f	Mock-Route	\N	\N	\N	\N
\.


--
-- Data for Name: schema_meta; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.schema_meta (key, subsystem, last_executed, executed, pending) FROM stdin;
schema_meta	core	003_100_to_110	{000_base,001_14_to_15,002_15_to_1,003_100_to_110}	{}
schema_meta	oauth2	002_15_to_10	{000_base_oauth2,001_14_to_15,002_15_to_10}	{}
schema_meta	acl	001_14_to_15	{000_base_acl,001_14_to_15}	{}
schema_meta	jwt	001_14_to_15	{000_base_jwt,001_14_to_15}	\N
schema_meta	basic-auth	001_14_to_15	{000_base_basic_auth,001_14_to_15}	\N
schema_meta	key-auth	001_14_to_15	{000_base_key_auth,001_14_to_15}	\N
schema_meta	rate-limiting	003_10_to_112	{000_base_rate_limiting,001_14_to_15,002_15_to_10,003_10_to_112}	{}
schema_meta	hmac-auth	001_14_to_15	{000_base_hmac_auth,001_14_to_15}	\N
schema_meta	response-ratelimiting	002_15_to_10	{000_base_response_rate_limiting,001_14_to_15,002_15_to_10}	{}
\.


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.services (id, created_at, updated_at, name, retries, protocol, host, port, path, connect_timeout, write_timeout, read_timeout, tags) FROM stdin;
274a9154-6480-431f-ab37-a7a4cd8018df	2019-05-31 14:46:38+00	2019-06-01 19:03:35+00	API	5	https	api-internal.airbuk.com	443	/	60000	60000	60000	\N
a9a21678-ad68-414c-8daf-2837c29f066d	2019-06-01 18:09:06+00	2019-06-01 19:03:41+00	Mock	5	http	api.icndb.com	80	/jokes/random	60000	60000	60000	\N
\.


--
-- Data for Name: snis; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.snis (id, created_at, name, certificate_id, tags) FROM stdin;
9254010f-71b5-4488-bfe6-362b0315cdc6	2019-05-31 07:38:29+00	 *.airbuk.com	6737f2e2-3e56-4dba-9220-99aa84b78768	\N
4bc6ede1-fd9d-408a-8296-e71c5ee8d4a3	2019-05-31 07:38:29+00	airbuk.com	6737f2e2-3e56-4dba-9220-99aa84b78768	\N
\.


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.tags (entity_id, entity_name, tags) FROM stdin;
6737f2e2-3e56-4dba-9220-99aa84b78768	certificates	\N
9254010f-71b5-4488-bfe6-362b0315cdc6	snis	\N
4bc6ede1-fd9d-408a-8296-e71c5ee8d4a3	snis	\N
72f0554f-0623-48d5-bdfc-0a73fd56aa4c	consumers	\N
2f4530a2-891d-41ba-80d4-fd64a5aba009	routes	\N
3c29d8e0-17ee-496d-873d-e42194f2ced1	routes	\N
fc66dbbb-75be-4829-bec7-01c3ac86c0ea	plugins	\N
a6997c80-5240-4e6c-8b86-3b449969c67d	plugins	\N
274a9154-6480-431f-ab37-a7a4cd8018df	services	\N
a9a21678-ad68-414c-8daf-2837c29f066d	services	\N
2dbbe332-d82d-481d-a95c-185486649b10	routes	\N
\.


--
-- Data for Name: targets; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.targets (id, created_at, upstream_id, target, weight, tags) FROM stdin;
\.


--
-- Data for Name: ttls; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.ttls (primary_key_value, primary_uuid_value, table_name, primary_key_name, expire_at) FROM stdin;
\.


--
-- Data for Name: upstreams; Type: TABLE DATA; Schema: public; Owner: operator
--

COPY public.upstreams (id, created_at, name, hash_on, hash_fallback, hash_on_header, hash_fallback_header, hash_on_cookie, hash_on_cookie_path, slots, healthchecks, tags) FROM stdin;
\.


--
-- Name: acls acls_cache_key_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.acls
    ADD CONSTRAINT acls_cache_key_key UNIQUE (cache_key);


--
-- Name: acls acls_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.acls
    ADD CONSTRAINT acls_pkey PRIMARY KEY (id);


--
-- Name: apis apis_name_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.apis
    ADD CONSTRAINT apis_name_key UNIQUE (name);


--
-- Name: apis apis_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.apis
    ADD CONSTRAINT apis_pkey PRIMARY KEY (id);


--
-- Name: basicauth_credentials basicauth_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.basicauth_credentials
    ADD CONSTRAINT basicauth_credentials_pkey PRIMARY KEY (id);


--
-- Name: basicauth_credentials basicauth_credentials_username_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.basicauth_credentials
    ADD CONSTRAINT basicauth_credentials_username_key UNIQUE (username);


--
-- Name: certificates certificates_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.certificates
    ADD CONSTRAINT certificates_pkey PRIMARY KEY (id);


--
-- Name: cluster_ca cluster_ca_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.cluster_ca
    ADD CONSTRAINT cluster_ca_pkey PRIMARY KEY (pk);


--
-- Name: cluster_events cluster_events_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.cluster_events
    ADD CONSTRAINT cluster_events_pkey PRIMARY KEY (id);


--
-- Name: consumers consumers_custom_id_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.consumers
    ADD CONSTRAINT consumers_custom_id_key UNIQUE (custom_id);


--
-- Name: consumers consumers_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.consumers
    ADD CONSTRAINT consumers_pkey PRIMARY KEY (id);


--
-- Name: consumers consumers_username_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.consumers
    ADD CONSTRAINT consumers_username_key UNIQUE (username);


--
-- Name: hmacauth_credentials hmacauth_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.hmacauth_credentials
    ADD CONSTRAINT hmacauth_credentials_pkey PRIMARY KEY (id);


--
-- Name: hmacauth_credentials hmacauth_credentials_username_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.hmacauth_credentials
    ADD CONSTRAINT hmacauth_credentials_username_key UNIQUE (username);


--
-- Name: jwt_secrets jwt_secrets_key_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.jwt_secrets
    ADD CONSTRAINT jwt_secrets_key_key UNIQUE (key);


--
-- Name: jwt_secrets jwt_secrets_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.jwt_secrets
    ADD CONSTRAINT jwt_secrets_pkey PRIMARY KEY (id);


--
-- Name: keyauth_credentials keyauth_credentials_key_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.keyauth_credentials
    ADD CONSTRAINT keyauth_credentials_key_key UNIQUE (key);


--
-- Name: keyauth_credentials keyauth_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.keyauth_credentials
    ADD CONSTRAINT keyauth_credentials_pkey PRIMARY KEY (id);


--
-- Name: locks locks_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.locks
    ADD CONSTRAINT locks_pkey PRIMARY KEY (key);


--
-- Name: oauth2_authorization_codes oauth2_authorization_codes_code_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_authorization_codes
    ADD CONSTRAINT oauth2_authorization_codes_code_key UNIQUE (code);


--
-- Name: oauth2_authorization_codes oauth2_authorization_codes_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_authorization_codes
    ADD CONSTRAINT oauth2_authorization_codes_pkey PRIMARY KEY (id);


--
-- Name: oauth2_credentials oauth2_credentials_client_id_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_credentials
    ADD CONSTRAINT oauth2_credentials_client_id_key UNIQUE (client_id);


--
-- Name: oauth2_credentials oauth2_credentials_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_credentials
    ADD CONSTRAINT oauth2_credentials_pkey PRIMARY KEY (id);


--
-- Name: oauth2_tokens oauth2_tokens_access_token_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_tokens
    ADD CONSTRAINT oauth2_tokens_access_token_key UNIQUE (access_token);


--
-- Name: oauth2_tokens oauth2_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_tokens
    ADD CONSTRAINT oauth2_tokens_pkey PRIMARY KEY (id);


--
-- Name: oauth2_tokens oauth2_tokens_refresh_token_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_tokens
    ADD CONSTRAINT oauth2_tokens_refresh_token_key UNIQUE (refresh_token);


--
-- Name: plugins plugins_cache_key_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.plugins
    ADD CONSTRAINT plugins_cache_key_key UNIQUE (cache_key);


--
-- Name: plugins plugins_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.plugins
    ADD CONSTRAINT plugins_pkey PRIMARY KEY (id);


--
-- Name: ratelimiting_metrics ratelimiting_metrics_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.ratelimiting_metrics
    ADD CONSTRAINT ratelimiting_metrics_pkey PRIMARY KEY (identifier, period, period_date, service_id, route_id);


--
-- Name: response_ratelimiting_metrics response_ratelimiting_metrics_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.response_ratelimiting_metrics
    ADD CONSTRAINT response_ratelimiting_metrics_pkey PRIMARY KEY (identifier, period, period_date, service_id, route_id);


--
-- Name: routes routes_name_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_name_key UNIQUE (name);


--
-- Name: routes routes_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_pkey PRIMARY KEY (id);


--
-- Name: schema_meta schema_meta_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.schema_meta
    ADD CONSTRAINT schema_meta_pkey PRIMARY KEY (key, subsystem);


--
-- Name: services services_name_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.services
    ADD CONSTRAINT services_name_key UNIQUE (name);


--
-- Name: services services_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: snis snis_name_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.snis
    ADD CONSTRAINT snis_name_key UNIQUE (name);


--
-- Name: snis snis_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.snis
    ADD CONSTRAINT snis_pkey PRIMARY KEY (id);


--
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (entity_id);


--
-- Name: targets targets_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.targets
    ADD CONSTRAINT targets_pkey PRIMARY KEY (id);


--
-- Name: ttls ttls_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.ttls
    ADD CONSTRAINT ttls_pkey PRIMARY KEY (primary_key_value, table_name);


--
-- Name: upstreams upstreams_name_key; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.upstreams
    ADD CONSTRAINT upstreams_name_key UNIQUE (name);


--
-- Name: upstreams upstreams_pkey; Type: CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.upstreams
    ADD CONSTRAINT upstreams_pkey PRIMARY KEY (id);


--
-- Name: acls_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX acls_consumer_id_idx ON public.acls USING btree (consumer_id);


--
-- Name: acls_group_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX acls_group_idx ON public.acls USING btree ("group");


--
-- Name: basicauth_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX basicauth_consumer_id_idx ON public.basicauth_credentials USING btree (consumer_id);


--
-- Name: certificates_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX certificates_tags_idx ON public.certificates USING gin (tags);


--
-- Name: cluster_events_at_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX cluster_events_at_idx ON public.cluster_events USING btree (at);


--
-- Name: cluster_events_channel_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX cluster_events_channel_idx ON public.cluster_events USING btree (channel);


--
-- Name: consumers_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX consumers_tags_idx ON public.consumers USING gin (tags);


--
-- Name: consumers_username_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX consumers_username_idx ON public.consumers USING btree (lower(username));


--
-- Name: hmacauth_credentials_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX hmacauth_credentials_consumer_id_idx ON public.hmacauth_credentials USING btree (consumer_id);


--
-- Name: jwt_secrets_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX jwt_secrets_consumer_id_idx ON public.jwt_secrets USING btree (consumer_id);


--
-- Name: jwt_secrets_secret_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX jwt_secrets_secret_idx ON public.jwt_secrets USING btree (secret);


--
-- Name: keyauth_credentials_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX keyauth_credentials_consumer_id_idx ON public.keyauth_credentials USING btree (consumer_id);


--
-- Name: locks_ttl_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX locks_ttl_idx ON public.locks USING btree (ttl);


--
-- Name: oauth2_authorization_codes_authenticated_userid_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_authorization_codes_authenticated_userid_idx ON public.oauth2_authorization_codes USING btree (authenticated_userid);


--
-- Name: oauth2_authorization_credential_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_authorization_credential_id_idx ON public.oauth2_authorization_codes USING btree (credential_id);


--
-- Name: oauth2_authorization_service_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_authorization_service_id_idx ON public.oauth2_authorization_codes USING btree (service_id);


--
-- Name: oauth2_credentials_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_credentials_consumer_id_idx ON public.oauth2_credentials USING btree (consumer_id);


--
-- Name: oauth2_credentials_secret_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_credentials_secret_idx ON public.oauth2_credentials USING btree (client_secret);


--
-- Name: oauth2_tokens_authenticated_userid_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_tokens_authenticated_userid_idx ON public.oauth2_tokens USING btree (authenticated_userid);


--
-- Name: oauth2_tokens_credential_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_tokens_credential_id_idx ON public.oauth2_tokens USING btree (credential_id);


--
-- Name: oauth2_tokens_service_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX oauth2_tokens_service_id_idx ON public.oauth2_tokens USING btree (service_id);


--
-- Name: plugins_api_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_api_id_idx ON public.plugins USING btree (api_id);


--
-- Name: plugins_consumer_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_consumer_id_idx ON public.plugins USING btree (consumer_id);


--
-- Name: plugins_name_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_name_idx ON public.plugins USING btree (name);


--
-- Name: plugins_route_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_route_id_idx ON public.plugins USING btree (route_id);


--
-- Name: plugins_run_on_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_run_on_idx ON public.plugins USING btree (run_on);


--
-- Name: plugins_service_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_service_id_idx ON public.plugins USING btree (service_id);


--
-- Name: plugins_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX plugins_tags_idx ON public.plugins USING gin (tags);


--
-- Name: ratelimiting_metrics_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX ratelimiting_metrics_idx ON public.ratelimiting_metrics USING btree (service_id, route_id, period_date, period);


--
-- Name: routes_service_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX routes_service_id_idx ON public.routes USING btree (service_id);


--
-- Name: routes_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX routes_tags_idx ON public.routes USING gin (tags);


--
-- Name: services_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX services_tags_idx ON public.services USING gin (tags);


--
-- Name: snis_certificate_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX snis_certificate_id_idx ON public.snis USING btree (certificate_id);


--
-- Name: snis_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX snis_tags_idx ON public.snis USING gin (tags);


--
-- Name: tags_entity_name_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX tags_entity_name_idx ON public.tags USING btree (entity_name);


--
-- Name: tags_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX tags_tags_idx ON public.tags USING gin (tags);


--
-- Name: targets_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX targets_tags_idx ON public.targets USING gin (tags);


--
-- Name: targets_target_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX targets_target_idx ON public.targets USING btree (target);


--
-- Name: targets_upstream_id_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX targets_upstream_id_idx ON public.targets USING btree (upstream_id);


--
-- Name: ttls_primary_uuid_value_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX ttls_primary_uuid_value_idx ON public.ttls USING btree (primary_uuid_value);


--
-- Name: upstreams_tags_idx; Type: INDEX; Schema: public; Owner: operator
--

CREATE INDEX upstreams_tags_idx ON public.upstreams USING gin (tags);


--
-- Name: certificates certificates_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER certificates_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.certificates FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: consumers consumers_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER consumers_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.consumers FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: cluster_events delete_expired_cluster_events_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER delete_expired_cluster_events_trigger AFTER INSERT ON public.cluster_events FOR EACH STATEMENT EXECUTE PROCEDURE public.delete_expired_cluster_events();


--
-- Name: plugins plugins_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER plugins_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.plugins FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: routes routes_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER routes_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.routes FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: services services_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER services_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.services FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: snis snis_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER snis_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.snis FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: targets targets_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER targets_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.targets FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: upstreams upstreams_sync_tags_trigger; Type: TRIGGER; Schema: public; Owner: operator
--

CREATE TRIGGER upstreams_sync_tags_trigger AFTER INSERT OR DELETE OR UPDATE OF tags ON public.upstreams FOR EACH ROW EXECUTE PROCEDURE public.sync_tags();


--
-- Name: acls acls_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.acls
    ADD CONSTRAINT acls_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: basicauth_credentials basicauth_credentials_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.basicauth_credentials
    ADD CONSTRAINT basicauth_credentials_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: hmacauth_credentials hmacauth_credentials_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.hmacauth_credentials
    ADD CONSTRAINT hmacauth_credentials_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: jwt_secrets jwt_secrets_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.jwt_secrets
    ADD CONSTRAINT jwt_secrets_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: keyauth_credentials keyauth_credentials_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.keyauth_credentials
    ADD CONSTRAINT keyauth_credentials_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: oauth2_authorization_codes oauth2_authorization_codes_credential_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_authorization_codes
    ADD CONSTRAINT oauth2_authorization_codes_credential_id_fkey FOREIGN KEY (credential_id) REFERENCES public.oauth2_credentials(id) ON DELETE CASCADE;


--
-- Name: oauth2_authorization_codes oauth2_authorization_codes_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_authorization_codes
    ADD CONSTRAINT oauth2_authorization_codes_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.services(id) ON DELETE CASCADE;


--
-- Name: oauth2_credentials oauth2_credentials_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_credentials
    ADD CONSTRAINT oauth2_credentials_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: oauth2_tokens oauth2_tokens_credential_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_tokens
    ADD CONSTRAINT oauth2_tokens_credential_id_fkey FOREIGN KEY (credential_id) REFERENCES public.oauth2_credentials(id) ON DELETE CASCADE;


--
-- Name: oauth2_tokens oauth2_tokens_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.oauth2_tokens
    ADD CONSTRAINT oauth2_tokens_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.services(id) ON DELETE CASCADE;


--
-- Name: plugins plugins_api_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.plugins
    ADD CONSTRAINT plugins_api_id_fkey FOREIGN KEY (api_id) REFERENCES public.apis(id) ON DELETE CASCADE;


--
-- Name: plugins plugins_consumer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.plugins
    ADD CONSTRAINT plugins_consumer_id_fkey FOREIGN KEY (consumer_id) REFERENCES public.consumers(id) ON DELETE CASCADE;


--
-- Name: plugins plugins_route_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.plugins
    ADD CONSTRAINT plugins_route_id_fkey FOREIGN KEY (route_id) REFERENCES public.routes(id) ON DELETE CASCADE;


--
-- Name: plugins plugins_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.plugins
    ADD CONSTRAINT plugins_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.services(id) ON DELETE CASCADE;


--
-- Name: routes routes_service_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_service_id_fkey FOREIGN KEY (service_id) REFERENCES public.services(id);


--
-- Name: snis snis_certificate_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.snis
    ADD CONSTRAINT snis_certificate_id_fkey FOREIGN KEY (certificate_id) REFERENCES public.certificates(id);


--
-- Name: targets targets_upstream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: operator
--

ALTER TABLE ONLY public.targets
    ADD CONSTRAINT targets_upstream_id_fkey FOREIGN KEY (upstream_id) REFERENCES public.upstreams(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

